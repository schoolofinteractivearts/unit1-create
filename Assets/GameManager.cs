﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    // These are the image prefab that we instantiate. Remember in program world, it always starts with 0
    public GameObject image0Prefab;
    public GameObject image1Prefab;
    public GameObject image2Prefab;

    // This is the integer counting how many cookies we have.
    public int cookies;


    // Update is called once per frame
    void Update()
    {

        // If Statement to deal with mouse clicking action
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {

            // Increase the cookies by 1;
            cookies += 1;

            // Play the sound of clicking;
            GetComponent<AudioSource>().Play();


            // This If statement deals with the event when cookies reach 10
            if (cookies == 10) {

                // Show the first image
                Instantiate(image0Prefab);


                // Show message in console
                Debug.Log("You got a cursor! It begins making additional cookies!");
            }

            // This If statement deals with the event when cookies reach 20
            if (cookies == 20) {

                // Show the second image
                Instantiate(image1Prefab);

                // Show message in console
                Debug.Log("You got a grandma! She begins making additional cookies!");
            }

            // This If statement deals with the event when cookies reach 30
            if (cookies == 30) {

                // Show the third image
                Instantiate(image2Prefab);

                // Show message in console
                Debug.Log("You got a cookie farm! It begins producing additional cookies!");
            }

        }

    }
}

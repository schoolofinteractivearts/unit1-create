# Unit 1 Modify

## Requirement
1. Your project should track mouse input and increase the number of cookies by one every time the left mouse button is pressed.
1. On reaching 10 cookies, the console should display the message, “You got a cursor! It begins making additional cookies!” A graphic of a cursor should appear and a new sound should play.
1. On reaching 20 cookies, the console should display the message, “You got a grandma! She begins making additional cookies!” An image of a grandma should appear and a new sound should play.
1. On reaching 30 cookies, the console should display the message, “You got a cookie farm! It begins producing additional cookies!” An image of a farm should appear and a new sound should play.



## Skills
1. GetComponent
2. variable declaration
3. if/else statements
4.  using ==
5.  input using Input.GetKeyDown with if statements in Update
6. int
7.  GameObject, AudioSource 
8. data types,
9.   \ + - * / operators,
10.  understanding and usage of Update function, Destroy() and Instantiate()
11. Debug.Log()
